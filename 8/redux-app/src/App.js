import React from 'react';
import {connect} from 'react-redux';

class App extends React.Component{

    handleIncrement = () => {
        this.props.dispatch({type: 'INCREMENT'})
    };

    handleDecrement = () => {
        this.props.dispatch({type: 'DECREMENT'})
    };

    render() {
        return (
            <div>
                <span>{this.props.value}</span>
                <button onClick={this.handleIncrement}>+</button>
                <button onClick={this.handleDecrement}>-</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        value: state.value,
        products: state.products
    }
};

export default connect(mapStateToProps)(App);
