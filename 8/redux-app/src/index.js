import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker'
import {Provider} from 'react-redux'
import {createStore} from 'redux'

const initState = {
    value: 10,
    products: ['A']
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case 'INCREMENT': return {
            value: state.value + 1
        };
        case 'DECREMENT': return  {
            value: state.value - 1
        };
        default:
            return state
    }
};

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

