import React, {Component} from 'react';

class App extends Component {

    state = { // object
        tasks: [ // array
            { id: 1, name: 'JS' }, // object
            { id: 2, name: 'CSS' },
            { id: 3, name: 'PHP' },
            { id: 4, name: 'PHP' },
        ],
        newTask: ''
    };

    addTaskHandle = (e) => {
        // const value = document.getElementById('input-task').value;

        console.log(this.state.newTask);
        const newTaskObj = {
            id: Date.now(),
            name: this.state.newTask
        };

        // let tasks = this.state.tasks;
        // tasks.push(newTaskObj);

        let tasks = [...this.state.tasks, newTaskObj];


        this.setState({tasks});
    };

    onChangeHandle = (e) => {
        // console.log(e.target.value);
        const newTask = e.target.value;
        this.setState({newTask}) // this.state.newTask = e.target.value;

    };


    render() {
        // console.log('render');
        // for (let i=0; i<this.state.tasks.length; i++) {
        //     const item = this.state.tasks[i];
        //
        //     console.log("ele: " + item)
        // }

        // this.state.tasks.map(function (item, i) {
        //     console.log("ele: " + this.state.tasks[i])
        // });

        // this.state.tasks.map( task => console.log("ele: " + task) );


        // function array(list) {
        //
        //     return {
        //         map: (callbackAction) => {
        //             // console.log(callbackAction);
        //             callbackAction(5) // argument
        //
        //             // callbackAction(); // exec , calling
        //         }
        //     }
        // }
        //
        // let list = [
        //     1,2,3,4
        // ];
        //
        //
        // let tempList = array(list);
        //
        //
        // tempList.map(this.addTaskHandle);

        return (
            <div className="container mt-5">
                <div className="text-center">
                    <input type="text" value={this.state.newTask} onChange={this.onChangeHandle} className="form-control m-5" id="input-task"/>
                    <button className="btn btn-success" onClick={this.addTaskHandle}>Add</button>
                </div>
                <ul className="list-group mt-5">
                    {this.state.tasks.map((task, id) => (<li key={task.id} className="list-item">{task.name}</li>))}
                </ul>
            </div>
        );
    }
}

export default App;
