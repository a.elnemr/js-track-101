// document.getElementById('root').innerHTML = '<h1> JS style</h1>';

import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './App';

const elm = document.getElementById('root');

ReactDOM.render(<App />, elm);

// select element and assign html content
// $('div').html('');