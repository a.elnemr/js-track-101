import React from 'react';

function LiUser({user, x, updateName}) {
    return (
        <li key={user.id}>{user.name + x} <button onClick={updateName}></button></li>
    )
}

export default LiUser;
