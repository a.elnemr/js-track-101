import React, {Component} from 'react';
import LiUser from "./LiUser";

class App extends Component {
    state = {
        users: [
            {
                id: 1,
                name: 'Ahmed Mohamed',
            },
            {
                id: 2,
                name: 'Ali Hassan'
            }
        ],
        newUser: {},
        name: ''
    };

    updateName = () => {
        this.setState({name: 'name'});
    };

    render() {
        // const demo = {
        //     name: 'Test',
        //     age: 88
        // };
        // // const name = demo.name;
        // const {name, age} = demo;
        //
        const {users} = this.state;
        // const users = this.state.users;
        console.log(this.state.name);
        return (
            <ul>
                {users.map(user => <LiUser updateName={this.updateName} user={user} x={7}/>)}
            </ul>
        );
    }
}


export default App;
